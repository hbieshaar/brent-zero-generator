const zeros = require('../zero_generator.js');
const zero = zeros.zero;

let counter = 0;
function count(f) {
    counter = 0;
    return function (x) {
        counter++;
        return f(x);
    }
}


const f_01 = x => Math.sin(x) - 0.5 * x;

test(f_01 + ' in [1, 2]', () => {
    expect(zero(count(f_01), 1, 2)).toBeCloseTo(1.895494, 6);
    expect(counter).toBe(8);
})

const f_02 = x => 2.0 * x - Math.exp(-x);

test(f_02 + ' in [0, 1]', () => {
    expect(zero(count(f_02), 0, 1)).toBeCloseTo(0.351734, 6);
    expect(counter).toBe(8);
})

const f_03 = x => x * Math.exp(-x);

test(f_03 + ' in [-1, 0.5]', () => {
    expect(zero(count(f_03), -1, 0.5)).toBeCloseTo(0.000000, 6);
    expect(counter).toBe(13);
})

const f_04 = x => Math.exp(x) - 1.0 / 100.0 / x / x;

test(f_04 + ' in [0.0001, 20]', () => {
    expect(zero(count(f_04), 0.0001, 2)).toBeCloseTo(0.095345, 6);
    expect(counter).toBe(15);
})

const f_05 = x => (x + 3) * (x - 1) * (x - 1);

test(f_05 + ' in [-5, 2]', () => {
    expect(zero(count(f_05), -5, 2)).toBeCloseTo(-3.000000, 6);
    expect(counter).toBe(14);
})

const f_06 = x => Math.sin(x) - x;

test(f_06 + ' in [-1, 3]', () => {
    expect(zero(count(f_06), -5, 2)).toBeCloseTo(0.000000, 6);
    expect(counter).toBe(82);
})

const f_07 = x => x > Math.PI ? 1 : -1;

test(f_07 + ' in [3, 4]', () => {
    expect(zero(count(f_07), 3, 4)).toBeCloseTo(355/113, 6);
    expect(counter).toBe(51);
})

const f_08 = x => x < 3 ? -1 : 2;

test(f_08 + ' in [2, 4]', () => {
    expect(zero(count(f_08), 2, 4)).toBeCloseTo(3, 6);
    expect(counter).toBe(65);
})

